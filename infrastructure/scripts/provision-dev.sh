#!/usr/bin/env bash

# Install ansible
echo 'Install ansible'
apt-add-repository ppa:ansible/ansible
apt-get -y update
apt-get -y install ansible

echo 'Copy ansible config files'
# Copy ansible config files
cp /src/infrastructure/ansible.cfg /etc/ansible/ansible.cfg
cp /src/infrastructure/hosts /etc/ansible/hosts 

echo 'Add ssh key for vagrant user'
# Add ssh key agent
sudo ansible-playbook /src/infrastructure/playbooks/devserver.yml -c local