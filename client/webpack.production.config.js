const webpack = require( 'webpack' );
const WebpackStrip = require( 'strip-loader' );
const process = require( "process" );

let deployPath = '';

process.argv.forEach( ( item ) =>
{
	if ( item.indexOf( "deploy-path=" ) === 0 )
	{
		deployPath = item.substr( 12, item.length );
	}
});

if ( deployPath === '' )
{
    throw new Error( "`deploy-path` argument not specified" );
}

// main config file
let config = require( './webpack.config' );

// output path
config.output.path = deployPath;

// cdn path
config.output.publicPath = '/assets/';

config.plugins.push(
	new webpack.DefinePlugin( {
		'process.env':
		{
			NODE_ENV: JSON.stringify( 'production' )
		}
	} )
);

config.module.loaders.push( {
	test: /\.js$/,
	loader: WebpackStrip.loader( 'console.log' )
} );

module.exports = config;