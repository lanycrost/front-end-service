const path = require( 'path' );
const conf = require( '../webpack.config' );

module.exports = conf.getConfig( path.resolve( __dirname, 'src' ) );