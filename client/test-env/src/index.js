import './util/render';
import './util/lib';
import './util/App';

/**
 * @fileoverview util/render.jsx Test react components
 import ReactDOM from 'react-dom';
 import React from 'react';
 import '../../../src/css/normalize.pcss';

 ReactDOM.render(
	 <div>Hello World</div>,
	 document.getElementById( 'react-root' )
 );
 */

/**
 * @fileoverview util/lib.js Test modules and libraries
 */
