const path = require( 'path' );

// webpack plugins
const HtmlPlugin = require( 'html-webpack-plugin' );

// load postcss plugins
const autoprefixer = require( 'autoprefixer' );
const customProperties = require( 'postcss-custom-properties' );
const minmax = require( 'postcss-media-minmax' );
const nesting = require( 'postcss-nesting' );

// css variables
const cssVariables = require( './src/const/css-vars' );

const getConfig = function ( root )
{
	return {

		// entry file
		entry:
		[
			'babel-polyfill',
			'whatwg-fetch',
			'./app.js'
		],

		// application path
		context: root,

		// resolve default extensions
		resolve:
		{
			extensions: [ '', '.js', '.jsx' ]
		},

		// output options
		output:
		{
			path: './build/',
			filename: '[hash].bundle.js',
			publicPath: '/'
		},

		// list of webpack loaders
		module:
		{
			loaders:
			[
				{
					exclude: /(node_modules)/,
					loader: 'babel',
					test: /\.(js|jsx)$/
				},
				{
					exclude: /(node_modules)/,
					test: /\.pcss/,
					loaders:
					[
						'style?singleton', 'css?modules', 'postcss'
					]
				},
				{
					test: /\.(png|jpg|jpeg|gif|ttf|eot|svg|woff(2)?)$/,
					loader: 'file'
				}
			],

			preLoaders:
			[
				{
					exclude: /(node_modules)/,
					test: /\.(js|jsx)$/,
					loader: 'eslint-loader'
				}
			]
		},

		plugins: [
			new HtmlPlugin( {
				template: 'index.html'
			} )
		],

		eslint:
		{
			configFile: path.resolve( __dirname, '.eslintrc' )
		},

		/**
		 * Return postcss configuration
		 * @return {Array} Return array of postCss plugins
		 */
		postcss: function ()
		{
			return [
				autoprefixer(),
				customProperties( {
					variables: cssVariables
				} ),
				minmax(),
				nesting()
			];
		}
	};
};

// Export configuration object by this root
module.exports = getConfig( path.resolve( __dirname, 'src' ) );

// Allow reuse of this configuration in different paths
module.exports.getConfig = getConfig;
