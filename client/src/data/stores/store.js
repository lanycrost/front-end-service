import { createStore, compose } from 'redux';
import reducers from '../reducers';
import schema from './schema';

let store = createStore( reducers, schema );

if ( !process.env.NODE_ENV || process.env.NODE_ENV === 'development' )
{
	 const enhancers = compose(
		window.devToolsExtension ?
			window.devToolsExtension() :
			( f ) =>
			{
				return f;
			}
	);

	store = createStore( reducers, schema, enhancers );
}

export default store;

