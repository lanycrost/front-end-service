import React from 'react';

/**
 * @class Root
 */
class Root extends React.Component
{

	static propTypes = {
		children: React.PropTypes.node
	};

	/**
	 * @constructor
	 */
	constructor()
	{
		super();

		this.state = {};
	}

	/**
	 * Render component
	 * @return {XML} Render component
	 */
	render()
	{
		return (
			<div>
				{ this.props.children }
			</div>
		);
	}
}

export default Root;