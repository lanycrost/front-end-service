import Main from './index.jsx';
import snippet from '../snippet/routes';

export default {

	childRoutes:
	[
		{
			path: '',
			component: Main,
			childRoutes: [
				snippet
			]
		}
	]
};