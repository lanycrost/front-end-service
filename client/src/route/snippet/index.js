import React from 'react';
import { connect } from 'react-redux';
import action from '../../data/actions/action';

/**
 * @class Snippet
 */
class Snippet extends React.Component
{

	/**
	 * @param {object} state - Redux state
	 * @param {object} ownProps - ownProps
	 * @return {{state: *, ownProps: *}} - return
	 */
	static mapStateToProps( state, ownProps )
	{
		return {
			state: state,
			ownProps: ownProps
		};
	}

	/**
	 * @param {object} dispatch - Dispatcher
	 * @param {object} ownProps - ownProps
	 * @return {{action: (function())}} - return
	 */
	static mapDispatchToProps( dispatch, ownProps )
	{
		return {
			action: () =>
			{
				dispatch( action( ownProps ) );
			}
		};
	}

	/**
	 * @constructor
	 */
	constructor()
	{
		super();
	}

	/**
	 * Render component
	 * @return {XML} xml
	 */
	render()
	{
		return (
			<div>
				Hello World
			</div>
		);
	}
}

connect(
	Snippet.mapStateToProps,
	Snippet.mapDispatchToProps
)( Snippet );

export default Snippet;