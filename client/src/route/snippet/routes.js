import Snippet from './index';

export default {
	path: '/snippet',
	component: Snippet
};