import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import { Router, hashHistory } from 'react-router';
import MainRoutes from './route/root/routes';
import store from './data/stores/store';
import './css/normalize.pcss';

const Root = () =>
{
	return (
		<Provider store={ store }>
			<Router
				history={ hashHistory }
				routes={ MainRoutes }
			/>
		</Provider>
	);
};

ReactDOM.render(
	<Root />,
	document.getElementById( 'react-root' ) );