<?php

error_reporting(E_ALL);
ini_set('display_errors', 1);

// Base dir constant
use Scalify\Http\HttpKernel;
use Scalify\Middleware\AuthMiddleware;
use Scalify\ServiceToolKit\Service;
use Scalify\ServiceToolKit\ServiceContainer;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Scalify\Di\Container;

define( 'BASE', __DIR__ );

// require util file
require_once BASE . '/app/util.php';

// require autoload file
require_once BASE . '/vendor/autoload.php';

// service configuration
$services = ServiceContainer::readServices( getContents( '/src/Service/services.yaml' ) );
$serviceContainer = new ServiceContainer();
$entities= [];

foreach ( $services as $service )
{
    $uppercaseName = ucfirst($service['name']);
    $specification = getContents( "/src/Service/{$uppercaseName}/specification.yaml" );

    $serviceContainer->setService(
        $service['name'],
        new Service( $service['name'], $specification, $service['config'] ) );

    if ( $service[ 'config' ][ 'has_entity' ] )
    {
        $entities[] = BASE . "/src/Service/{$uppercaseName}/Entity";
    }
}

$dbParams = require_once BASE . '/app/config/database.php';

$config = \Doctrine\ORM\Tools\Setup::createAnnotationMetadataConfiguration( $entities, true, null, null, false );
$entityManager = \Doctrine\ORM\EntityManager::create( $dbParams, $config );

// DI container
$container = new Container();
$container->set( Container::SERVICE_CONTAINER, $serviceContainer );
$container->set( Container::ENTITY_CONTAINER, $entityManager );

// Http components
$request = Request::createFromGlobals();
$response = new Response();
$kernel = new HttpKernel( $request, $response, $container );

// enable cors
$response->headers->set('Access-Control-Allow-Origin', '*');

// register middlewares
$kernel->registerMiddleWare( new AuthMiddleware() )
        ->handleRequest();