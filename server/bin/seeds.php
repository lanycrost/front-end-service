<?php

define( "BASE", __DIR__ . '/../' );

require_once BASE . 'vendor/autoload.php';
require_once BASE . 'app/util.php';

use Symfony\Component\Console\Helper\HelperSet;
use Scalify\ServiceToolKit\ServiceContainer;

// service configuration
$services = ServiceContainer::readServices( getContents( '/src/Service/services.yaml' ) );
$entities= [];

foreach ( $services as $service )
{
    if ( $service[ 'config' ][ 'has_entity' ] )
    {
        $name = ucfirst( $service['name'] );
        $entities[] = BASE . "src/Service/{$name}/Entity/";
    }
}

$dbParams = require_once BASE . 'app/config/database.php';

$config = \Doctrine\ORM\Tools\Setup::createAnnotationMetadataConfiguration( $entities, false, null, null, false );
$entityManager = \Doctrine\ORM\EntityManager::create( $dbParams, $config );

$commands = array();

$helperSet = \Doctrine\ORM\Tools\Console\ConsoleRunner::createHelperSet($entityManager);

if ( ! ($helperSet instanceof HelperSet)) {
    foreach ($GLOBALS as $helperSetCandidate) {
        if ($helperSetCandidate instanceof HelperSet) {
            $helperSet = $helperSetCandidate;
            break;
        }
    }
}

\Doctrine\ORM\Tools\Console\ConsoleRunner::run($helperSet, $commands);