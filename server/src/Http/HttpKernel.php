<?php

namespace Scalify\Http;

use Scalify\ServiceToolKit\ServiceContainer;
use Scalify\Di\Container;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Exception\MethodNotAllowedException;
use Symfony\Component\Routing\Exception\ResourceNotFoundException;
use Symfony\Component\Routing\RequestContext;
use Symfony\Component\Routing\Matcher\UrlMatcher;

class HttpKernel implements HttpKernelInterface
{

    /**
     * @var array
     */
    private $middlewares = [];

    /**
     * @var Request
     */
    private $request;

    /**
     * @var Response
     */
    private $response;

    /**
     * @var Container
     */
    private $container;

    /**
     * @param string $service
     * @param array $options
     */
    private function pushRouteToMiddle( string $service, array $options )
    {
        $instance = '\\Scalify\\Service\\' . ucfirst( $service ) . '\\Route\\' . $options['operationId'];

        array_push( $this->middlewares, new $instance() );
    }

    /**
     * @param RouteInterface $middleware
     * @param mixed $next
     */
    private function invokeMiddleware( RouteInterface $middleware, $next )
    {
        try {
            call_user_func(
                [ $middleware, 'index' ],
                $this->request,
                $this->response,
                $this->container,
                $next
            );
        }
        catch ( HttpKernelException $exception )
        {
            $payload = new Payload( $this->response );

            // send error to user
            $payload->setError( $exception->getMessage() )->send();
        }
    }

    public function next()
    {
        return function () {
            $this->invokeMiddleware( next( $this->middlewares ), $this->next() );
        };
    }

    /**
     *
     */
    private function invokeMiddlewares()
    {
        $this->invokeMiddleware( current( $this->middlewares ), $this->next() );
    }

    /**
     * HttpKernel constructor.
     * @param Request $request
     * @param Response $response
     * @param Container $container
     */
    public function __construct( Request $request, Response $response, Container $container )
    {
        $this->request = $request;
        $this->response = $response;
        $this->container = $container;
    }

    /**
     * @param RouteInterface $middleware
     * @return $this
     */
    public function registerMiddleWare(RouteInterface $middleware)
    {
        array_push( $this->middlewares, $middleware );

        return $this;
    }

    /**
     *
     */
    public function handleRequest()
    {
        /**
         * @var ServiceContainer $serviceContainer
         */
        $serviceContainer = $this->container->get( Container::SERVICE_CONTAINER );

        // create request context
        $context = new RequestContext('/', $this->request->getMethod() );

        // init route matcher
        $matcher = new UrlMatcher($serviceContainer->getMergedRouteCollection(), $context);

        try
        {
            // match route
            // remove / from request path
            // this should be done for request paths too.
            $routingParams = $matcher->match( rtrim( $this->request->getPathInfo(), '/' ) );

            // make request method lowercase
            $requestMethod = strtolower( $this->request->getMethod() );

            foreach ( $routingParams['_operations'] as $method => $options )
            {
                $method = strtolower($method);

                if( $requestMethod === $method )
                {
                    // save current operation configuration
                    // no need for other operations
                    $routingParams['_operation'] = $options;
                    unset( $routingParams['_operations'] );

                    // store routing params
                    $this->container->set(Container::ROUTING_PARAMS, $routingParams);

                    // push route to middlewares
                    $this->pushRouteToMiddle( $routingParams['_service'], $routingParams['_operation'] );

                    // start invoking middlewares
                    $this->invokeMiddlewares();

                    return;
                }
            }

            throw new ResourceNotFoundException('Resource not found');
        }
        catch ( ResourceNotFoundException $exception )
        {
            print 'Url not found on our server!';
        }
        catch ( MethodNotAllowedException $exception )
        {
            print 'Method is not allowed';
        }

    }
}