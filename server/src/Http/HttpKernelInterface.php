<?php

namespace Scalify\Http;

use Scalify\Di\Container;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

interface HttpKernelInterface
{
    /**
     * Initialize kernel
     * @param Request $request
     * @param Response $response
     * @param Container $container
     */
    public function __construct( Request $request, Response $response, Container $container );

    /**
     * @return mixed
     */
    public function handleRequest();

    /**
     * Registed middlewares
     * @param RouteInterface $middleware
     */
    public function registerMiddleWare( RouteInterface $middleware );
}