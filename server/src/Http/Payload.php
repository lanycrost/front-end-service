<?php

namespace Scalify\Http;

use Symfony\Component\HttpFoundation\Response;

class Payload
{

    /**
     * @var array
     */
    private $payload;

    /**
     * @var Response
     */
    private $response;

    /**
     * Payload constructor.
     * @param Response $response
     */
    public function __construct( Response $response )
    {
        $this->response = $response;

        // initialize payload data
        $this->payload = [
            'error' => false,
            'payload' => []
        ];
    }

    /**
     * @param $item
     * @return $this
     */
    public function setItem( $item )
    {
        $this->payload['payload']['item'] = $item;

        return $this;
    }

    /**
     * @param array $items
     * @return $this
     */
    public function setItems( array $items )
    {
        $this->payload['payload']['items'] = $items;

        return $this;
    }

    /**
     * @return string
     */
    public function create():string
    {
        return json_encode( $this->payload );
    }

    public function send()
    {
        // send json content type header
        $this->response->headers->set('Content-Type', 'application/json');

        // send response
        $this->response->setContent( $this->create() )->send();
    }

    /**
     * @param string $error
     * @return $this
     */
    public function setError( string $error )
    {
        $this->payload['error'] = true;
        $this->payload['payload'] = $error;

        return $this;
    }
}