<?php

namespace Scalify\Http;

use Symfony\Component\HttpFoundation\Request;

class Schema
{

    /**
     * @var Request
     */
    private $request;

    /**
     * Schema constructor.
     * @param Request $request
     */
    public function __construct( Request $request )
    {
        $this->request = $request;
    }

    /**
     * @return mixed|bool
     */
    public function getPayload()
    {
        $json = json_decode( $this->request->getContent() );

        if( isset( $json->payload ) )
        {
            return $json->payload;
        }

        return false;
    }
}