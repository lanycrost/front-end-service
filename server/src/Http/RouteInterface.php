<?php

namespace Scalify\Http;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Scalify\Di\Container;

interface RouteInterface
{
    /**
     * @param Request $request
     * @param Response $response
     * @param Container $container
     * @param mixed $next
     * @return void
     */
    public function index( Request $request, Response $response, Container $container, $next );
}