<?php

namespace Scalify\ServiceToolKit;


use Symfony\Component\Routing\Route;
use Symfony\Component\Routing\RouteCollection;
use Symfony\Component\Yaml\Yaml;
use Scalify\ServiceToolKit\Exception\SpecificationParserException;

class SpecificationParser
{

    /**
     * Specification string
     * @var string
     */
    private $specification;

    /**
     * Service name
     * @var string
     */
    private $serviceName;

    /**
     * SpecificationParser constructor.
     * @param string $serviceName
     * @param string $specification
     */
    public function __construct( string $serviceName, string $specification )
    {
        $this->serviceName = $serviceName;
        $this->specification = $specification;
    }

    /**
     * @throws SpecificationParserException
     * @return RouteCollection
     */
    public function createRouteCollection():RouteCollection
    {
        $parsedSpecifications = Yaml::parse( $this->specification );
        $routeCollection = new RouteCollection();

        // check for basePath
        if( ! isset( $parsedSpecifications['basePath'] ) )
        {
            throw new SpecificationParserException('Base path is not specified');
        }

        $basePath = $parsedSpecifications['basePath'];

        if ( ! isset( $parsedSpecifications['paths'] ) )
        {
            throw new SpecificationParserException('Paths are empty or not defined');
        }

        foreach ( $parsedSpecifications['paths'] as $path => $pathMethods )
        {
            $routeParams = [
                '_service' => $this->serviceName,
                '_operations' => []
            ];

            $allowedMethods = [];

            foreach ( $pathMethods as $method => $options )
            {
                if ( ! isset( $options['operationId'] ) )
                {
                    throw new SpecificationParserException(
                        "Operation id is not defined for 
                        service {$this->serviceName}: 
                        path {$path}:
                        method {$method}
                        "
                    );
                }

                array_push( $allowedMethods, $method );
                $routeParams['_operations'][$method] = $options;
            }

            $route = new Route( rtrim( $basePath . $path, '/' ), $routeParams );
            $route->setMethods( $allowedMethods );

            $routeCollection->add( $path, $route );
        }

        return $routeCollection;
    }


}