<?php

namespace Scalify\ServiceToolKit;

use Scalify\ServiceToolKit\Exception\SpecificationParserException;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBag;
use Symfony\Component\Routing\RouteCollection;

class Service
{

    /**
     * Service name
     * @var string
     */
    private $serviceName;

    /**
     * @var ParameterBag
     */
    private $serviceConfig;

    /**
     * Yaml specification string
     * @var string
     */
    private $specification;

    /**
     * RouteCollection of service
     * @var SpecificationParser
     */
    private $specificationParser;

    /**
     * Service constructor.
     * @param string $serviceName
     * @param string $specification
     * @param array $serviceConfig
     */
    public function __construct( string $serviceName, string $specification, array $serviceConfig )
    {
        // store specification string
        $this->specification = $specification;

        // store service name
        $this->serviceName = $serviceName;

        // store service config
        $this->serviceConfig = new ParameterBag( $serviceConfig );

        // create parser instance
        $this->specificationParser = new SpecificationParser( $serviceName, $specification );
    }

    /**
     * Get route collection
     * @return RouteCollection
     * @throws SpecificationParserException
     */
    public function getRouteCollection( ):RouteCollection
    {
        return $this->specificationParser->createRouteCollection();
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->serviceName;
    }

    /**
     * @param string $name
     * @return array
     */
    public function getConfig( $name )
    {
        if( $this->serviceConfig->has( $name ) )
        {
            return $this->serviceConfig->get( $name );
        }

        return null;
    }
}