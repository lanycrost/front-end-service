<?php

namespace Scalify\ServiceToolKit;

use Symfony\Component\DependencyInjection\ParameterBag\ParameterBag;
use Symfony\Component\Routing\RouteCollection;
use Symfony\Component\Yaml\Yaml;

class ServiceContainer
{

    /**
     * @var ParameterBag
     */
    private $container;

    /**
     * @param string $serviceConfigFile
     * @return array
     */
    public static function readServices( string $serviceConfigFile )
    {
        return Yaml::parse( $serviceConfigFile );
    }

    /**
     * ServiceContainer constructor.
     */
    public function __construct()
    {
        $this->container = new ParameterBag();
    }

    /**
     * @return RouteCollection
     */
    public function getMergedRouteCollection():RouteCollection
    {
        $services = $this->container->all();

        $routeCollection = new RouteCollection();

        /**
         * @var Service $service
         */
        foreach ( $services as $serviceName => $service )
        {
            $routeCollection->addCollection( $service->getRouteCollection() );
        }

        return $routeCollection;
    }

    /**
     * @param string $name
     * @param Service $service
     * @return void
     */
    public function setService( string $name, Service $service )
    {
        $this->container->set( $name, $service );
    }

    /**
     * @param string $name
     * @return Service
     */
    public function getService( string $name ):Service
    {
        /**
         * @var Service $service
         */
        $service = $this->container->get( $name );

        return $service;
    }
}