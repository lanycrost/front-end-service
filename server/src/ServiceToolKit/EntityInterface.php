<?php

namespace Scalify\ServiceToolKit;


interface EntityInterface
{
    /**
     * Returns Entity model
     * @return mixed
     */
    public function getModel();
}