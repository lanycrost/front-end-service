<?php

namespace Scalify\Service\App\Route;

use Scalify\FileUtils\File;
use Scalify\Http\RouteInterface;
use Scalify\Di\Container;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class RenderApp implements RouteInterface
{
    public function index(Request $request, Response $response, Container $container, $next)
    {
        $file = new File();
        $index = '/public/assets/index.html';

        if ( $file->exists( BASE . $index ) )
        {
            $content = getContents( $index );
        }
        else
        {
            $content = 'Something went wrong while rendering application';
        }

        $response->setContent( $content )->send();
    }
}