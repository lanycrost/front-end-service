<?php

namespace Scalify\Service\User\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="userPayments")
 */
class UserPayments
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="integer", options={"unsigned":true})
     */
    protected $id;

    /**
     * @ORM\OneToOne(targetEntity="Scalify\Service\User\Entity\User")
     * @ORM\JoinColumn(name="userId", referencedColumnName="id")
     * @ORM\Column(type="integer", nullable=false)
     */
    protected $userId;

    /**
     * @ORM\Column(type="string", nullable=false)
     */
    protected $cardToken;

    /**
     * @ORM\Column(type="string", nullable=false)
     */
    protected $customerToken;

    /**
     * @ORM\Column(type="string", nullable=false)
     */
    protected $subscriptionId;

    /**
     * @param int $userId
     */
    public function setUserId( int $userId )
    {
        $this->userId = $userId;
    }

    /**
     * @param string $cardToken
     */
    public function setCardToken( string $cardToken )
    {
        $this->cardToken = $cardToken;
    }

    /**
     * @param string $customerToken
     */
    public function setCustomerToken( string $customerToken )
    {
        $this->customerToken = $customerToken;
    }

    /**
     * @param string $subscriptionId
     */
    public function setSubscriptionId( string $subscriptionId )
    {
        $this->subscriptionId = $subscriptionId;
    }

    /**
     * @return string
     */
    public function getCardToken(): string
    {
        return $this->cardToken;
    }

    /**
     * @return string
     */
    public function getCustomerToken(): string
    {
        return $this->customerToken;
    }

    /**
     * @return string
     */
    public function getSubscriptionId(): string
    {
        return $this->subscriptionId;
    }
}