<?php

namespace Scalify\Service\User\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="user")
 */
class User
{

    /**
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="integer", options={"unsigned":true})
     */
    protected $id;

    /**
     * @ORM\Column(type="string", nullable=false)
     */
    protected $email;

    /**
     * @ORM\Column(type="string", nullable=false)
     */
    protected $password;

    /**
     * @ORM\Column(type="string", nullable=false)
     */
    protected $address;

    /**
     * @ORM\Column(type="string", nullable=false)
     */
    protected $fullName;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    protected $campaignId;

    /**
     * @ORM\ManyToOne(targetEntity="Scalify\Service\User\Entity\UserTypes")
     * @ORM\JoinColumn(name="type", referencedColumnName="id")
     */
    protected $type;

    /**
     * @ORM\Column(type="boolean", nullable=false)
     */
    protected $isActive;

    /**
     * @ORM\Column(type="integer", nullable=false)
     */
    protected $createdBy;

    /**
     * @ORM\Column(type="integer", nullable=false)
     */
    protected $createdAt;

    /**
     * @ORM\Column(type="integer", nullable=false)
     */
    protected $updatedAt;

    /**
     * @return int
     */
    public function getId():int
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getEmail():string
    {
        return $this->email;
    }

    /**
     * @return string
     */
    public function getPassword():string
    {
        return $this->password;
    }

    /**
     * @return string
     */
    public function getAddress():string
    {
        return $this->address;
    }

    /**
     * @return string
     */
    public function getFullName():string
    {
        return $this->fullName;
    }

    /**
     * @return string
     */
    public function getCampaignId():string
    {
        return $this->campaignId;
    }

    /**
     * @return bool
     */
    public function isActive():bool
    {
        return $this->isActive == 1;
    }

    /**
     * @return UserTypes
     */
    public function getType(): UserTypes
    {
        return $this->type;
    }

    /**
     * @return mixed
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * @param string $address
     */
    public function setAddress( string $address )
    {
        $this->address = $address;
    }

    /**
     * @param string $campaignId
     */
    public function setCampaignId( string $campaignId = null )
    {
        $this->campaignId = $campaignId;
    }

    /**
     * @param int $createdBy
     */
    public function setCreatedBy( int $createdBy )
    {
        $this->createdBy = $createdBy;
    }

    /**
     * @param string $createdAt
     */
    public function setCreatedAt( string $createdAt )
    {
        $this->createdAt = $createdAt;
    }

    /**
     * @param string $email
     */
    public function setEmail( string $email )
    {
        $this->email = $email;
    }

    /**
     * @param string $fullName
     */
    public function setFullName( string $fullName )
    {
        $this->fullName = $fullName;
    }

    /**
     * @param bool $isActive
     */
    public function setIsActive( bool $isActive )
    {
        $this->isActive = $isActive;
    }

    /**
     * @param string $password
     */
    public function setPassword( string $password )
    {
        $this->password = $password;
    }

    /**
     * @param UserTypes $type
     */
    public function setType( UserTypes $type )
    {
        $this->type = $type;
    }

    /**
     * @param int $updatedAt
     */
    public function setUpdatedAt( int $updatedAt )
    {
        $this->updatedAt = $updatedAt;
    }

    /**
     * @return array
     */
    public function getModel()
    {
        return [
            'address' => $this->address,
            'campaignId' => $this->campaignId,
            'fullName' => $this->fullName,
            'email' => $this->email,
            'isActive' => $this->isActive,
            'createdAt' => $this->createdAt
        ];
    }
}