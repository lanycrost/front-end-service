<?php

namespace Scalify\Service\User\Route;

use Doctrine\ORM\EntityManager;
use Scalify\Http\HttpKernelException;
use Scalify\Http\RouteInterface;
use Scalify\Service\User\Entity\User;
use Scalify\Service\User\Entity\UserData;
use Scalify\Di\Container;
use Scalify\Service\User\Entity\UserTypes;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Scalify\Http\Schema;
use Scalify\Http\Payload;

class RegisterUser implements RouteInterface
{
    public function index(Request $request, Response $response, Container $container, $next)
    {
        // get json body
        $schema = new Schema( $request );
        $data = $schema->getPayload();

        // create payload
        $payload = new Payload( $response );

        /**
         * @var EntityManager $entityManager
         */
        $entityManager = $container->get( Container::ENTITY_CONTAINER );

        /**
         * @var UserTypes $userType
         */
        $userType = $entityManager
            ->getRepository( 'Scalify\Service\User\Entity\UserTypes' )
            ->findOneBy( array('type' => $data->type) );

        /**
         * @var User $user
         */
        $user = new User();

        $user->setAddress( $data->address );
        $user->setFullName( $data->fullName );
        $user->setCampaignId( $data->campaignId );
        $user->setPassword( password_hash( $data->password, PASSWORD_DEFAULT ) );
        $user->setEmail( $data->email );
        $user->setCreatedAt( time() );
        $user->setUpdatedAt( time() );
        $user->setCreatedBy( $container->get( Container::APP_USER )->getId() );
        $user->setIsActive( 1 );
        $user->setType( $userType );

        try
        {
            $entityManager->persist( $user );
            $entityManager->flush();

            $payload->setItem( 'OK' )->send();
        }
        catch ( \Exception $exception )
        {
            throw new HttpKernelException( 'Something is wrong when creating user!' );
        }
    }
}