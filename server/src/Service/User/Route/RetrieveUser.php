<?php

namespace Scalify\Service\User\Route;

use Doctrine\ORM\EntityManager;
use Scalify\Di\Container;
use Scalify\Http\HttpKernelException;
use Scalify\Http\Payload;
use Scalify\Http\RouteInterface;
use Scalify\Service\User\Entity\User;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class RetrieveUser implements RouteInterface
{
    public function index(Request $request, Response $response, Container $container, $next)
    {
        $payload = new Payload( $response );
        $routingParams = $container->get( Container::ROUTING_PARAMS );

        /**
         * @var EntityManager $entityContainer
         */
        $entityContainer = $container->get( Container::ENTITY_CONTAINER );

        /**
         * @var User $user
         */
        $user = $entityContainer
                    ->getRepository('Scalify\Service\User\Entity\User')
                    ->find( $routingParams['userId'] );

        if ( ! $user )
        {
            throw new HttpKernelException('User not found');
        }

        $payload->setItem( $user->getModel() )->send();
    }
}