<?php

namespace Scalify\Service\User\Route;

use Doctrine\ORM\EntityManager;
use Scalify\Di\Container;
use Scalify\Http\HttpKernelException;
use Scalify\Http\Payload;
use Scalify\Http\RouteInterface;
use Scalify\Http\Schema;
use Scalify\Service\User\Entity\User;
use Scalify\Service\User\Entity\UserPayments;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Stripe\Stripe;
use Stripe\Token;
use Stripe\Customer;

class AddCreditCard implements RouteInterface
{
    public function index(Request $request, Response $response, Container $container, $next)
    {
        /**
         * @var User $appUser
         */
        $appUser = $container->get( Container::APP_USER );

        /**
         * @var EntityManager $entityContainer
         */
        $entityContainer = $container->get( Container::ENTITY_CONTAINER );

        $schema = new Schema( $request );
        $payload = new Payload( $response );
        $requestPayload = $schema->getPayload();

        $payment = $entityContainer
            ->getRepository('Scalify\Service\User\Entity\UserPayments')
            ->findOneBy([ 'userId' => $appUser->getId() ]);

        if ( $payment )
        {
            throw new HttpKernelException('Credit card already exists');
        }

        // set test api key
        Stripe::setApiKey("sk_test_RH2PvwjmNWp4YOReRy9AHhVE");

        // save token
        $token = Token::create([
            'card' => [
                'number' => $requestPayload->number,
                'exp_month' => $requestPayload->exp_month,
                'exp_year' => $requestPayload->exp_year,
                'cvc' => $requestPayload->cvc
            ]
        ]);

        // save customer
        $customer = Customer::create([
            'description' => 'Customer ' . $appUser->getEmail(),
            'source' => $token->id
        ]);

        // save info to database
        $userPayment = new UserPayments();

        $userPayment->setCustomerToken( $customer->id );
        $userPayment->setCardToken( $token->id );
        $userPayment->setUserId( $appUser->getId() );
        $userPayment->setSubscriptionId('');

        try
        {
            $entityContainer->persist( $userPayment );
            $entityContainer->flush();
        }
        catch ( \Exception $exception )
        {
            throw new HttpKernelException('Something went wrong while saving card. Try again');
        }

    }
}