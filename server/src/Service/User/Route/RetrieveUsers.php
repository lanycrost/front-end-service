<?php

namespace Scalify\Service\User\Route;

use Doctrine\ORM\EntityManager;
use Scalify\Di\Container;
use Scalify\Http\Payload;
use Scalify\Http\RouteInterface;
use Scalify\Service\User\Entity\User;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class RetrieveUsers implements RouteInterface
{
    public function index(Request $request, Response $response, Container $container, $next)
    {
        $payload = new Payload( $response );
        $finalUsers = [];


        /**
         * @var EntityManager $entityContainer
         */
        $entityContainer = $container->get( Container::ENTITY_CONTAINER );

        $users = $entityContainer
            ->getRepository('Scalify\Service\User\Entity\User')
            ->findAll();

        /**
         * @var User $user
         */
        foreach ( $users as $user )
        {
            array_push( $finalUsers, $user->getModel() );
        }

        $payload->setItems( $finalUsers )->send();
    }
}