<?php

namespace Scalify\Service\User\Route;


use Scalify\Http\Payload;
use Scalify\Http\RouteInterface;
use Scalify\Di\Container;
use Scalify\Service\User\Entity\User;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class RetrieveProfile implements RouteInterface
{
    public function index(Request $request, Response $response, Container $container, $next)
    {
        $payload = new Payload( $response );

        /**
         * @var User $user
         */
        $user = $container->get( Container::APP_USER );

        $payload->setItem( $user->getModel() )->send();
    }
}