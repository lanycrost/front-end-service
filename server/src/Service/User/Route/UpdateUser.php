<?php

namespace Scalify\Service\User\Route;

use Scalify\Http\HttpKernelException;
use Scalify\Http\RouteInterface;
use Scalify\Di\Container;
use Scalify\Service\User\Entity\User;
use Scalify\Service\User\Entity\UserTypes;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Doctrine\ORM\EntityManager;
use Scalify\Http\Schema;
use Scalify\Http\Payload;

class UpdateUser implements RouteInterface
{
    public function index(Request $request, Response $response, Container $container, $next)
    {
        // get json body
        $schema = new Schema( $request );
        $data = $schema->getPayload();

        // create payload
        $payload = new Payload( $response );

        /**
         * @var EntityManager $entityManager
         */
        $entityManager = $container->get( Container::ENTITY_CONTAINER );

        /**
         * @var User $appUser
         */
        $appUser = $container->get( Container::APP_USER );

        /**
         * @var User $user
         */
        $user = $entityManager
            ->getRepository( 'Scalify\Service\User\Entity\User' )
            ->findOneBy( [ 'id' => $container->get( Container::ROUTING_PARAMS )[ 'userId' ] ] );

        if ( isset( $data->address ) )
        {
            $user->setAddress( $data->address );
        }
        if ( isset( $data->campaignId ) )
        {
            $user->setCampaignId( $data->campaignId );
        }
        if ( isset( $data->fullName ) )
        {
            $user->setFullName( $data->fullName );
        }
        if ( isset( $data->email ) )
        {
            $user->setEmail( $data->email );
        }
        if ( isset( $data->password ) )
        {
            $user->setPassword( password_hash( $data->password, PASSWORD_DEFAULT ) );
        }
        if ( isset( $data->type ) )
        {
            /**
             * @var UserTypes $userType
             */
            $userType = $entityManager
                ->getRepository( 'Scalify\Service\User\Entity\UserTypes' )
                ->findOneBy( array('type' => $data->type) );

            $user->setType( $userType );
        }

        $user->setUpdatedAt( time() );

        try
        {
            $entityManager->merge( $user );
            $entityManager->flush();

            $payload->setItem( 'OK' )->send();
        }
        catch ( \Exception $exception )
        {
            throw new HttpKernelException( 'Something is wrong when update user info!' );
        }
    }
}