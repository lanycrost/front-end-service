<?php

namespace Scalify\Service\Media\Route;

use Scalify\Di\Container;
use Scalify\FileUtils\File;
use Scalify\Http\RouteInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Scalify\Http\Payload;

class DeleteFile implements RouteInterface
{

    public function index(Request $request, Response $response, Container $container, $next)
    {
        $file = new File();

        $routingParams = $container->get( Container::ROUTING_PARAMS );

        $payload = new Payload( $response );

        $pathToFile = BASE . '/public/serve/' . $routingParams[ 'fileName' ];

        if ( $file->exists( $pathToFile ) )
        {
            $file->removeFile( $pathToFile );
            $payload->setItem( $routingParams['fileName'] )->send();
        }
        else
        {
            $payload->setError('Resource not found')->send();
        }
    }
}