<?php

namespace Scalify\Service\Media\Route;

use Scalify\Di\Container;
use Scalify\FileUtils\File;
use Scalify\Http\Payload;
use Scalify\Http\RouteInterface;
use Scalify\Http\Schema;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class UploadFile implements RouteInterface
{
    public function index(Request $request, Response $response, Container $container, $next)
    {
        // get json body
        $schema = new Schema( $request );
        $data = $schema->getPayload();

        $file = new File();

        // create payload
        $payload = new Payload( $response );

        // file content
        $fileContent = $data->fileContent;

        // unique name
        $name = File::createUniqueName( File::getExtension( $fileContent ) );

        if( $file->createFromBase64( $fileContent, BASE . '/public/serve/' . $name ) )
        {
            $payload->setItem([
                'fileUrl' => '/serve/' . $name
            ]);
        }
        else
        {
            $payload->setError('Failed to write file');
        }

        $payload->send();
    }
}