<?php

namespace Scalify\Service\Auth\Route;

use Scalify\Http\Payload;
use Scalify\Http\RouteInterface;
use Scalify\Di\Container;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\HttpFoundation\Session\Storage\Handler\NativeFileSessionHandler;
use Symfony\Component\HttpFoundation\Session\Storage\NativeSessionStorage;

class RemoveSession implements RouteInterface
{
    public function index(Request $request, Response $response, Container $container, $next)
    {
        $payload = new Payload( $response );

        // init Session
        $storage = new NativeSessionStorage([], new NativeFileSessionHandler( BASE . '/tmp'));
        $session = new Session($storage);

        // clear Session storage
        $session->clear();

        // send correct response
        $payload->setItem('OK')->send();
    }
}