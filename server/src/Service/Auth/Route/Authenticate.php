<?php

namespace Scalify\Service\Auth\Route;

use Doctrine\ORM\EntityManager;
use Scalify\Http\HttpKernelException;
use Scalify\Http\Payload;
use Scalify\Http\RouteInterface;
use Scalify\Http\Schema;
use Scalify\Service\Auth\Entity\AccessToken;
use Scalify\Service\User\Entity\User;
use Scalify\Di\Container;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class Authenticate implements RouteInterface
{
    public function index(Request $request, Response $response, Container $container, $next)
    {
        /**
         * @var EntityManager $entityContainer
         */
        $entityContainer = $container->get( Container::ENTITY_CONTAINER );

        $schema = new Schema( $request );
        $payload = new Payload($response);
        $data = $schema->getPayload();

        // login credentials
        $email = $data->email;
        $password = $data->password;

        /**
         * @var User $user
         * find user by email
         */
        $user = $entityContainer
            ->getRepository('Scalify\Service\User\Entity\User')
            ->findOneBy( array('email' => $email) );

        if( ! $user )
        {
            throw new HttpKernelException('User not found with provided email');
        }

        if ( ! $user->isActive() )
        {
            throw new HttpKernelException('User to be activated!');
        }

        // check if password is
        if( ! password_verify( $password, $user->getPassword() ) )
        {
            throw new HttpKernelException('Something went wrong when logging in');
        }

        $userId = $user->getId();

        // generate token
        $token = md5( $userId . time() . mt_rand() );

        // create access token
        $accessToken = new AccessToken();

        $accessToken->setToken( $token );
        $accessToken->setUser( $user );

        try
        {
            $entityContainer->persist( $accessToken );
            $entityContainer->flush();

            $payload->setItem([
                'access_token' => $token
            ])->send();
        }
        catch ( \Exception $exception )
        {
            throw new HttpKernelException('Something went wrong when creating token');
        }
    }
}