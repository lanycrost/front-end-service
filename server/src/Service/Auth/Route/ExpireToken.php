<?php

namespace Scalify\Service\Auth\Route;

use Scalify\Http\Payload;
use Scalify\Http\RouteInterface;
use Scalify\Di\Container;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Doctrine\ORM\EntityManager;
use Scalify\Http\HttpKernelException;
use Scalify\Service\Auth\Entity\AccessToken;

class ExpireToken implements RouteInterface
{
    public function index(Request $request, Response $response, Container $container, $next)
    {
        $payload = new Payload($response);

        /**
         * @var EntityManager $entityContainer
         */
        $entityContainer = $container->get( Container::ENTITY_CONTAINER );

        /**
         * @var string $accessToken
         */
        $token = $request->query->get( 'access_token' );

        /**
         * Get AccessToken for this Session
         * @var Scalify\Service\Auth\Entity\AccessToken $accessToken
         */
        $accessToken = $entityContainer
            ->getRepository( 'Scalify\Service\Auth\Entity\AccessToken' )
            ->findOneBy( array('token' => $token) );

        // expire token
        $accessToken->setExpired( true );

        try
        {
            $entityContainer->merge( $accessToken );
            $entityContainer->flush();

            $payload->setItem('OK')->send();
        }
        catch ( \Exception $exception )
        {
            throw new HttpKernelException('Something went wrong when expire token');
        }
    }
}