<?php

namespace Scalify\Service\Auth\Route;

use Scalify\Http\HttpKernelException;
use Scalify\Http\Payload;
use Scalify\Http\RouteInterface;
use Scalify\Di\Container;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\Storage\NativeSessionStorage;
use Symfony\Component\HttpFoundation\Session\Storage\Handler\NativeFileSessionHandler;
use Symfony\Component\HttpFoundation\Session\Session;

class RetrieveSession implements RouteInterface
{
    public function index(Request $request, Response $response, Container $container, $next)
    {
        $payload = new Payload( $response );

        // init Session
        $storage = new NativeSessionStorage([], new NativeFileSessionHandler( BASE . '/tmp'));
        $session = new Session($storage);

        // get access token with Session
        $accessToken = $session->get('access_token');

        /**
         * Get AccessToken for this Session
         * @var Scalify\Service\Auth\Entity\AccessToken $accessToken
         */
        $auth = $container
            ->get( 'entityContainer' )
            ->getRepository( 'Scalify\Service\Auth\Entity\AccessToken' )
            ->findOneBy( array('token' => $accessToken) );

        // check access token correct
        if ( !empty( $accessToken ) && !empty($auth) )
        {

            // check access token not expired
            if ( !$auth->isExpired() )
            {
                // send correct request
                $payload->setItem($accessToken)->send();
            }
            else
            {
                throw new HttpKernelException( 'Access Token Expired!');
            }
        }
        else
        {
            throw new HttpKernelException( 'Access Token Expired!' );
        }

    }
}