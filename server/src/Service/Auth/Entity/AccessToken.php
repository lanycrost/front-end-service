<?php

namespace Scalify\Service\Auth\Entity;

use Doctrine\ORM\Mapping as ORM;
use Scalify\Service\User\Entity\User;

/**
 * @ORM\Entity
 * @ORM\Table(name="accessToken")
 */
class AccessToken
{

    /**
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="integer", options={"unsigned":true})
     */
    protected $id;

    /**
     * @ORM\Column(type="string", nullable=false)
     */
    protected $token;

    /**
     * @ORM\Column(type="boolean", nullable=false)
     */
    protected $expired;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */

    /**
     * @ORM\ManyToOne(targetEntity="Scalify\Service\User\Entity\User")
     * @ORM\JoinColumn(name="userId", referencedColumnName="id")
     */
    protected $user;

    /**
     * @ORM\Column(type="string", nullable=false)
     */
    protected $cratedAt;

    /**
     * AccessToken constructor.
     */
    public function __construct()
    {
        $this->cratedAt = time();
        $this->expired = 0;
    }

    /**
     * @return bool
     */
    public function isExpired():bool
    {
        return (bool)$this->expired;
    }

    /**
     * @param string $token
     */
    public function setToken( string $token )
    {
        $this->token = $token;
    }

    /**
     * @param boolean $expired
     */
    public function setExpired( bool $expired )
    {
        $this->expired = $expired;
    }

    /**
     * @param $user
     */
    public function setUser( User $user )
    {
        $this->user = $user;
    }

    public function getUser():User
    {
        return $this->user;
    }
}