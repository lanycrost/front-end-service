<?php

namespace Scalify\FileUtils;

class File
{

    /**
     * FileCreator constructor.
     */
    public function __construct()
    {

    }

    /**
     * @param string $content
     * @param string $outputPath
     * @return string
     */
    public function createFromBase64(string $content, string $outputPath)
    {
        $ifp = fopen($outputPath, 'wb');

        $data = explode(',', $content);

        $fw = fwrite($ifp, base64_decode($data[1]));

        fclose($ifp);

        return $fw;
    }

    /**
     * @param string $pathToFile
     */
    public function removeFile(string $pathToFile)
    {
        unlink( $pathToFile );
    }

    /**
     * @param string $pathToFile
     * @return bool
     */
    public function exists( string $pathToFile )
    {
        return file_exists( $pathToFile );
    }

    /**
     * @param string $content
     * @return string
     */
    public static function getExtension( string $content )
    {
        $info = getimagesize( $content );

        switch ( $info['mime'] )
        {
            case 'image/png':
                return 'png';
            case 'image/jpg':
                return 'jpg';
            case 'image/jpeg':
                return 'jpeg';
            default:
                return '';
        }
    }

    /**
     * @param string $ext
     * @return string
     */
    public static function createUniqueName( string $ext )
    {
        return md5( time() . uniqid() . mt_rand() ) . '.' . $ext;
    }
}