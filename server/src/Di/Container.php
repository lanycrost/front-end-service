<?php

namespace Scalify\Di;

use Symfony\Component\DependencyInjection\ParameterBag\ParameterBag;

class Container
{
    /**
     * @const
     */
    const SERVICE_CONTAINER = 'SERVICE_CONTAINER';

    /**
     * @const
     */
    const ENTITY_CONTAINER = 'ENTITY_CONTAINER';

    /**
     * @const
     */
    const ROUTING_PARAMS = 'ROUTING_PARAMS';

    /**
     * @const
     */
    const APP_USER = 'APP_USER';

    /**
     * @var Container
     */
    private $container;

    /**
     * Container constructor.
     */
    public function __construct()
    {
        $this->container = new ParameterBag();
    }

    /**
     * @param $id
     * @param $item
     * @throws \Exception
     */
    public function set( $id, $item )
    {
        if( $this->container->has( $id ) )
        {
            throw new \Exception( "{$id} is already defined in di container" );
        }

        $this->container->set( $id, $item );
    }

    /**
     * @param $id
     * @return mixed
     * @throws \Exception
     */
    public function get( $id )
    {
        if( ! $this->container->has( $id ) )
        {
            throw new \Exception( "{$id} is not defined in di container" );
        }

        return $this->container->get( $id );
    }
}