<?php

namespace Scalify\Middleware;

use Doctrine\ORM\EntityManager;
use Scalify\Di\Container;
use Scalify\Http\HttpKernelException;
use Scalify\Http\RouteInterface;
use Scalify\Service\Auth\Entity\AccessToken;
use Scalify\ServiceToolKit\ServiceContainer;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Scalify\Http\Payload;

class AuthMiddleware implements RouteInterface
{
    public function index(Request $request, Response $response, Container $container, $next)
    {
        /**
         * @var ServiceContainer $serviceContainer
         */
        $serviceContainer = $container->get( Container::SERVICE_CONTAINER );

        /**
         * @var EntityManager $entityManager
         */
        $entityManager = $container->get( Container::ENTITY_CONTAINER );


        $routingParams = $container->get( Container::ROUTING_PARAMS );
        $payload = new Payload( $response );

        $service = $serviceContainer->getService( $routingParams['_service'] );


        if( $service->getConfig('firewall_enabled') )
        {
            if( ! $request->query->has('access_token') )
            {
                throw new HttpKernelException('Access token required');
            }
            else
            {
                // find security option of operation
                if( ! isset( $routingParams['_operation']['security'] ) )
                {
                    throw new HttpKernelException("Operation security error");
                }

                $userTypes = $routingParams['_operation']['security'][0]['userTypes'];

                /**
                 * @var AccessToken $accessToken
                 */
                $accessToken = $entityManager
                    ->getRepository('Scalify\Service\Auth\Entity\AccessToken')
                    ->findOneBy( ['token' => $request->query->get('access_token')] );

                if( ! $accessToken || $accessToken->isExpired() )
                {
                    throw new HttpKernelException('Access token not found');
                }

                $user = $accessToken->getUser();

                // check if user has access to action
                if( ! in_array($user->getType()->getType(), $userTypes) )
                {
                     throw new HttpKernelException('You are not allowed for this action');
                }

                // save app user and go on
                $container->set(Container::APP_USER, $user);

                $next();
            }
        }
        else
        {
            $next();
        }
    }
}