<?php


namespace Scalify\Data;


use Doctrine\ORM\EntityManager;

interface ModelInterface
{

    public function __construct( EntityManager $entityManager );

}