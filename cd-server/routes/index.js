const express = require('express');
const fs = require( "fs" );
const path = require( "path" );
const exec = require( 'child_process' ).exec;
const router = express.Router();
const config = require( "../config" );
const logger = require( "../util/logger" );
const parseCommit = require( '../util/parseCommit' );
const clone = require( "../util/clone" );
const sendEmail = require( '../util/sendEmail' );

router.post('/', function(req, res, next) {
  const parsedData = parseCommit( req.body );
  const project = path.normalize( `${__dirname}/../storage/${config.appName}` );

  processCD( parsedData.branch )
      .then( () =>
      {
          if ( fs.existsSync( project ) )
          {
              exec(`ansible-playbook ${project}/infrastructure/playbooks/deploy.yml -c local`, ( err ) =>
              {
                  if ( err )
                  {
                      logger().error( err );
                  }

                  exec(`rm -rf ${project}`, ( err ) =>
                  {
                      if ( err )
                      {
                          logger().error( err );
                      }
                  });
              });
          }
      })
      .then( () =>
      {
          const regex = /<(.*?)>/ig;
          const email = regex.exec( parsedData.author.raw );
          const from = "khachatur.ashotyan@gmail.com";
          const to = email;
          const subject = "Bitbucket Commit Success";
          const message = `${ parsedData.commitHash } commit success integrate from server. Commit message: ${ parsedData.message }`;
          sendEmail( from, to, subject, message );
      })
      .then( () => {
          res.render('index', { title: 'Express' });
      } )
      .catch( ( err ) =>
      {
          const regex = /<(.*?)>/ig;
          const email = regex.exec( parsedData.author.raw );
          const from = "khachatur.ashotyan@gmail.com";
          const to = email;
          const subject = "Bitbucket Commit Error";
          const message = `${ parsedData.commitHash } commit throw error from server. Commit message: ${ parsedData.message }, Error Message: ${ err }`;
          sendEmail( from, to, subject, message );
      });
});

const processCD = ( branch ) =>
{
    return new Promise( (resolve, reject) =>
    {
        clone( branch, ( err, result ) =>
        {
            if (err)
            {
                return reject(err)
            }

            return resolve( result );
        });
    })
};

module.exports = router;