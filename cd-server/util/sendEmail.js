const nodemailer = require('nodemailer');
const config = require( "../config" );

module.exports = ( from, to, subject, message ) =>
{

    let transporter = nodemailer.createTransport({
        service: 'gmail',
        auth: {
            user: config.email,
            pass: config.password
        }
    });

    let mailOptions = {
        from: from,
        to: to,
        subject: subject,
        text: message,
    };

    transporter.sendMail( mailOptions, ( error, info ) =>
    {
        if (error)
        {
            return console.log(error);
        }

        console.log('Message %s sent: %s', info.messageId, info.response);
    });
};