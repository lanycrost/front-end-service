const fs = require( "fs" );
const exec = require('child_process').exec;
const path = require( "path" );
const config = require( '../config' );
const git = require("simple-git")();
const logger = require( "./logger" );

module.exports = ( branch, callback ) =>
{
    const clonedApp = path.normalize( `${__dirname}/../storage/${config.appName}` );

    if ( config.branch === branch )
    {
        if ( fs.existsSync( clonedApp ) )
        {
            exec('rm -r ' + clonedApp, function (err) {
                if (err)
                {
                    return logger().error( err );
                }

                clone( clonedApp, callback );
            });
        }
        else
        {
            clone( clonedApp, callback );
        }
    }
};

const clone = ( clonedApp, callback ) =>
{
    git.clone( config.repo, clonedApp, {}, ( err, result ) =>
    {
        if ( err )
        {
            logger().error( err );
            return callback( err );
        }

        return callback( err, result );
    })
};