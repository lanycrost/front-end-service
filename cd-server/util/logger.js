module.exports = () =>
{
    const winston = require( 'winston' );
    const fs = require( 'fs' );
    const path = require( "path" );
    const logDir = path.normalize( `${__dirname}/../logs` );

    if ( !fs.existsSync( logDir ) )
    {
        fs.mkdirSync( logDir );
    }

    const date = + new Date();

    return logger = new ( winston.Logger )({
        transports: [
            new ( winston.transports.File )({
                filename: `${logDir}/${date}.log`,
                timestamp: new Date( date )
            })
        ]
    });
};