const logger = require( './logger' );

module.exports = ( data ) =>
{
    const parsedData = {};
    const type = data.push;

    if ( typeof type === 'object' )
    {
        parsedData.branch = type.changes[ 0 ].new.name;
        parsedData.commitHash = type.changes[ 0 ].new.target.hash;
        parsedData.author = type.changes[ 0 ].new.target.author;
        parsedData.date = type.changes[ 0 ].new.target.date;
        parsedData.message = type.changes[ 0 ].new.target.message;

        logger().info( parsedData );

        return parsedData;
    }

    logger().error( "Incorrect" );
    throw new Error( "Incorrect" );
};